<!---
This template is for Product Analysis iteration planning.
---->

## Iteration: Name
<!---Update heading to iteration name---->

:calendar:

* Start date: `YYYY-MM-DD`
* End date: `YYYY-MM-DD`
<!---Update start and end dates---->

:link:

* Iteration: Name
  * [Iteration report](https://gitlab.com/groups/gitlab-data/-/iterations/)
  * [Planning issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/) (you're looking at it, its this issue :slight_smile: )
  * [Iteration board](https://gitlab.com/groups/gitlab-data/-/boards/2959103?iteration_id=)
* Previous Iteration: Name
  * [Iteration report](https://gitlab.com/groups/gitlab-data/-/iterations/)
  * [Planning issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/)
  * [Iteration board](https://gitlab.com/groups/gitlab-data/-/boards/2959103?iteration_id=)
* [Product Analysis Board - Current Iteration](https://gitlab.com/groups/gitlab-data/-/boards/2973914)

<!---Update names and links to correct iteration---->

### Working Days

Total working days in iteration: 10
<!---Update to number of days in iteration---->

| Team Member | Available Working Days | OOO Dates |
| --- | --- |--- |
| [Dave](@dpeterson1) |  |  |
| [Nicole](@nicolegalang) |  |  |
| [Neil](@nraisinghani) |  |  |
| [Emma](@eneuberger) |  |  |
| [Matthew](@matthewpetersen) |  |  |
| [Carolyn](@cbraza) |  |  |
<!---Update with your available working days and the dates you will be OOO (if applicable)---->

### Team Member Checklist

Please ensure the following prior to planning:
* Work breakdown is completed in an issue comment (1 comment per analyst)
  * Use work breakdown tables included in a drop-down below
* All issues are still open and are expected to still be open when next iteration begins
* All issues have a priority label
* All issues have a weight
* All issues have a workflow label
  * Hint: if the issue is planned for an iteration, it should have the `workflow::4 - scheduled` 
  label (unless it is blocked, in development, or in review)

<details>
  <summary>Work Breakdown</summary>

### Name

Working days: #

#### Issues

| Issue Title | Issue Number | Weight in _this_ Iteration (Total Weight) | Priority | Section & Group | Notes |
| --- | --- | --- | --- | --- | --- |
| **Potential Carryover from Previous Iteration** |
|  |  |  |  |  |  |
| **Expect to Complete** |
|  |  |  |  |  |  |
| **Top of the Backlog** |
|  |  |  |  |  |  |
| **Currently Blocked but Likely to Dedicate Time To** |
|  |  |  |  |  |  |

#### Estimated Totals

| Metric | Previous Iteration | This Iteration |
| --- | --- | --- |
| Completed issue count |  |  |
| Completed issue weight |  |  |
| Weight per working day |  |  |

</details>

<details>
  <summary>Priority Guide</summary>

  | Label | Priority |
  | --- | --- |
  | ~"pa-priority::1" | High and/or urgent |
  | ~"pa-priority::2" | Medium |
  | ~"pa-priority::3" | Low, non-urgent |

</details>

<details>
  <summary>Weight Guide</summary>

  Weight values are assigned to approximate the remaining time needed to complete the task.
  
  | Weight Value | Estimated Time to Complete |
  | --- | --- |
  | 1 | < 1 hour |
  | 2 | 2 hours |
  | 3 | 4 hours |
  | 5 | 1 day |
  | 8 | 2-3 days |
  | 13 | 1 week |
  | 21 | 2+ weeks |
  | 34 | 1+ month |
  | 55 | 1+ quarter |
  | 89 | 1+ year |
  | 144 | 1+ decade |

</details>

<details>
  <summary>Workflow Guide</summary>
  
  | Stage (Label) | Description | Completion Criteria |
  | --- | --- | --- |
  | `workflow::1 - triage` | New issue, being assessed | Requirements are complete and issue is assigned to an analyst |
  | `workflow::3 - scheduling` | Waiting for scheduling | Issue has an iteration |
  | `workflow::4 - scheduled` | Waiting for development | Work starts on the issue |
  | `workflow::5 - development` | Work is in-flight | Issue enters review |
  | `workflow::6 - review` | Waiting for or in review | Issue meets [criteria for closure](https://about.gitlab.com/handbook/product/product-analysis/team-processes/#checklist-for-closing-an-issue) |
  | `workflow::X - blocked` | Issue needs intervention that assignee can't perform | Work is no longer blocked |

</details>

---

### Final Velocity
<!---To be completed once iteration is complete---->
[Velocity calculations](https://about.gitlab.com/handbook/product/product-analysis/team-processes/#team-velocity-calculations)

| Analyst | Working Days | Completed Issue Count | Total & Completed Issue Weight | Total & Completed Velocity |
| --- | --- | --- | --- | --- |
| Dave | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |
| Nicole | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |
| Neil | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |
| Emma | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |
| Matthew | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |
| **Analyst Total** | **#** | **#** | **#** (**x in completed issues**, y in issues rolling over) | **#.#** (**#.# in completed issues**) |
| Carolyn | # | # | x+y (x in completed issues, y in issues rolling over) | #.# (#.# in completed issues) |

<!---Do not edit below---->
/label ~"product analysis" ~"workflow::3 - scheduling" ~"pa planning"
/assign @dpeterson1 @nicolegalang @nraisinghani @eneuberger @matthewpetersen @cbraza
/weight 0
/epic &513
